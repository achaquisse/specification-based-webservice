package com.achaquisse.info;

import java.util.Objects;

public class CarInfo {

    private String registrationNr;
    private String brand;
    private String model;
    private String color;

    public CarInfo(){
    }

    public CarInfo(String registrationNr, String brand, String model, String color) {
        this.registrationNr = registrationNr;
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public String getRegistrationNr() {
        return registrationNr;
    }

    public void setRegistrationNr(String registrationNr) {
        this.registrationNr = registrationNr;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarInfo car = (CarInfo) o;
        return registrationNr.equals(car.registrationNr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationNr);
    }

    @Override
    public String toString() {
        return "Car{" +
                "registrationNr='" + registrationNr + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
