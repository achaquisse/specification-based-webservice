package com.achaquisse.exception;

public class CarStoreApiException extends RuntimeException{

    public CarStoreApiException(String message) {
        super(message);
    }
}
