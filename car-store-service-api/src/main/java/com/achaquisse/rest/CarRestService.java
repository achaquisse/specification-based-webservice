package com.achaquisse.rest;

import com.achaquisse.info.CarInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("cars")
public interface CarRestService {

    @GET
    List<CarInfo> getAll();

    @GET
    @Path("{registrationNr}")
    CarInfo getByRegNumber(@PathParam("registrationNr") String registrationNr);

    @POST
    Response create(CarInfo car);

    @PUT
    @Path("{registrationNr}")
    void update(@PathParam("registrationNr") String registrationNr, CarInfo car);

    @DELETE
    @Path("{registrationNr}")
    void delete(@PathParam("registrationNr") String registrationNr);

    @GET
    @Path("count")
    long countAll();

}
