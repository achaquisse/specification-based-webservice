# Specification Based Webservice
* Nota: Para correr o cliente demo, deve antes correr a implementacao na porta 8080

# Referencias
https://microshed.org/microshed-testing/

https://blog.payara.fish/integration-testing-using-microprofile-testing-and-payara-micro

https://hub.docker.com/r/jboss/wildfly

http://cxf.apache.org/docs/jax-rs-client-api.html#JAX-RSClientAPI-MavenDependency

https://www.testcontainers.org/modules/databases/mariadb/

https://rieckpil.de/jakarta-ee-integration-tests-with-microshed-testing/