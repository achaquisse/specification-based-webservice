package com.achaquisse.domain;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import java.util.Optional;

@Repository(forEntity = Car.class)
public interface CarRepository extends EntityRepository<Car, Integer> {
    Optional<Car> findByRegistrationNr(String registrationNr);
}
