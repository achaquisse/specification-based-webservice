package com.achaquisse.domain;

import com.achaquisse.info.CarInfo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String registrationNr;
    private String brand;
    private String model;
    private String color;

    public static List<CarInfo> infoList(List<Car> cars) {
        List<CarInfo> carInfos = new ArrayList<>();
        for(Car car : cars){
            carInfos.add(info(car));
        }
        return carInfos;
    }

    public static CarInfo info(Car car) {
        return new CarInfo(car.registrationNr, car.brand, car.model, car.color);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegistrationNr() {
        return registrationNr;
    }

    public void setRegistrationNr(String registrationNr) {
        this.registrationNr = registrationNr;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
