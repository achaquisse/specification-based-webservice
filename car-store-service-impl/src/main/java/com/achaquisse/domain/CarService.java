package com.achaquisse.domain;

import com.achaquisse.exception.CarStoreApiException;
import com.achaquisse.info.CarInfo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class CarService {

    @Inject
    private CarRepository carRepository;

    public List<CarInfo> getAll() {
        List<Car> cars =  carRepository.findAll();
        return Car.infoList(cars);
    }

    public long countAll() {
        return carRepository.count();
    }

    public CarInfo getByRegNumber(String registrationNr) {
        Optional<Car> optionalCar = carRepository.findByRegistrationNr(registrationNr);
        if(optionalCar.isPresent()){
            return Car.info(optionalCar.get());
        } else {
            throw new CarStoreApiException(String.format("Car %s not found", registrationNr));
        }
    }

    public void create(CarInfo carInfo) {
        Car car = new Car();
        car.setBrand(carInfo.getBrand());
        car.setRegistrationNr(carInfo.getRegistrationNr());
        car.setColor(carInfo.getColor());
        car.setModel(carInfo.getModel());
        carRepository.save(car);
    }

    public void update(String registrationNr, CarInfo newCar) {
        Optional<Car> optionalCar = carRepository.findByRegistrationNr(registrationNr);
        if(optionalCar.isPresent()){
            Car car = optionalCar.get();
            car.setBrand(newCar.getBrand());
            car.setRegistrationNr(newCar.getRegistrationNr());
            car.setColor(newCar.getColor());
            car.setModel(newCar.getModel());
            carRepository.save(car);
        } else {
            throw new CarStoreApiException(String.format("Car %s not found", registrationNr));
        }
    }

    public void delete(String registrationNr) {
        Optional<Car> optionalCar = carRepository.findByRegistrationNr(registrationNr);
        if(optionalCar.isPresent()){
            Car car = optionalCar.get();
            carRepository.remove(car);
        } else {
            throw new CarStoreApiException(String.format("Car %s not found", registrationNr));
        }
    }

}
