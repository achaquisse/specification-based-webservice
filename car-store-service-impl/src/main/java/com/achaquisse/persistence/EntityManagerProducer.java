package com.achaquisse.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class EntityManagerProducer {

    private static final Logger logger = LoggerFactory.getLogger(EntityManagerProducer.class);

    private EntityManagerFactory emf;

    @PostConstruct
    private void createFactory() {
        logger.debug("Creating entity manager factory");

        String enableDevPu = System.getProperty("connection.enable.dev.pu");
        String devPu = "carDB";
        if (enableDevPu != null && enableDevPu.equals("true")) {
            devPu = "carDB_test";
        }
        Map<String, String> properties = composeProperties();
        if (properties.isEmpty()) {
            emf = Persistence.createEntityManagerFactory(devPu);
        } else {
            emf = Persistence.createEntityManagerFactory(devPu, properties);
        }
    }

    @PreDestroy
    private void closeFactory() {
        if (emf != null) {
            emf.close();
            logger.debug("Closing entity manager factory");
            emf = null;
        }
    }

    @Produces
    @Default
    @RequestScoped
    public EntityManager createEntityManager() {
        logger.debug("Creating entity manager and returning it");
        return emf.createEntityManager();
    }

    public void closeEntityManager(@Disposes @Default EntityManager em) {
        logger.debug("Attempting to close entity manager");
        if (em.isOpen()) {
            em.close();
            logger.debug("Closed entity manager");
        }
    }

    private Map<String, String> composeProperties() {
        Map<String, String> properties = new HashMap<>();

        //Can be set from outside

        String connectionUrl = (System.getenv("CONN_URL") != null) ? System.getenv("CONN_URL") : System.getProperty("connection.url");
        if (connectionUrl != null) {
            properties.put("hibernate.connection.url", connectionUrl);
        }
        String connectionUsername = (System.getenv("CONN_USERNAME") != null) ? System.getenv("CONN_USERNAME") : System.getProperty("connection.username");
        if (connectionUsername != null) {
            properties.put("hibernate.connection.username", connectionUsername);
        }
        String connectionPassword = (System.getenv("CONN_PASSWORD") != null) ? System.getenv("CONN_PASSWORD") : System.getProperty("connection.password");
        if (connectionPassword != null) {
            properties.put("hibernate.connection.password", connectionPassword);
        }
        String connectionDriver = (System.getenv("CONN_DRIVER") != null) ? System.getenv("CONN_DRIVER") : System.getProperty("connection.driver_class");
        if (connectionDriver != null) {
            properties.put("hibernate.connection.driver_class", connectionDriver);
        }
        String connectionDialect = (System.getenv("CONN_DIALECT") != null) ? System.getenv("CONN_DIALECT") : System.getProperty("hibernate.dialect");
        if (System.getProperty("hibernate.dialect") != null) {
            properties.put("hibernate.dialect", connectionDialect);
        }

        //Must be set only for unit testing

        String dbDropCreate = System.getProperty("database.drop-create");
        if (dbDropCreate != null) {
            if (dbDropCreate.equals("true")) {
                properties.put("javax.persistence.schema-generation.database.action", "drop-and-create");
            }
        }
        if (System.getProperty("connection.show_sql") != null) {
            properties.put("hibernate.show_sql", System.getProperty("connection.show_sql"));
        }
        if (System.getProperty("connection.format_sql") != null) {
            properties.put("hibernate.format_sql", System.getProperty("connection.format_sql"));
        }

        return properties;
    }

}
