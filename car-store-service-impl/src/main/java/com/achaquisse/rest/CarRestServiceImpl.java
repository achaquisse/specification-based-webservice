package com.achaquisse.rest;

import com.achaquisse.domain.CarService;
import com.achaquisse.info.CarInfo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class CarRestServiceImpl implements CarRestService {

    @Inject
    private CarService carStoreService;

    @Override
    public List<CarInfo> getAll() {
        return carStoreService.getAll();
    }

    @Override
    public CarInfo getByRegNumber(String registrationNr) {
        return carStoreService.getByRegNumber(registrationNr);
    }

    @Override
    public Response create(CarInfo carInfo) {
        carStoreService.create(carInfo);
        return Response.status(Response.Status.CREATED).build();
    }

    @Override
    public void update(String registrationNr, CarInfo newCar) {
        carStoreService.update(registrationNr, newCar);
    }

    @Override
    public void delete(String registrationNr) {
        carStoreService.delete(registrationNr);
    }

    @Override
    public long countAll() {
        return carStoreService.countAll();
    }

}
