package com.achaquisse.rest.bootstrap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class RootResource{
    @GET
    public Response status(){
        return Response.ok().entity("UP").build();
    }
}
