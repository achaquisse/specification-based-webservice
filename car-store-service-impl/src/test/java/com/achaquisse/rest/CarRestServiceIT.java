package com.achaquisse.rest;

import com.achaquisse.info.CarInfo;
import com.achaquisse.util.ApplicationConfig;
import org.junit.jupiter.api.Test;
import org.microshed.testing.SharedContainerConfig;
import org.microshed.testing.jaxrs.RESTClient;
import org.microshed.testing.jupiter.MicroShedTest;

import static org.junit.jupiter.api.Assertions.*;

@MicroShedTest
@SharedContainerConfig(ApplicationConfig.class)
public class CarRestServiceIT {

    @RESTClient
    public static CarRestService carService;

    @Test
    void testCreate() {
        assertEquals(2, carService.countAll());
        CarInfo car = new CarInfo("AEX-313-MC", "Toyota", "Allion", "Azul");
        carService.create(car);
        assertEquals(3, carService.countAll());
    }

    @Test
    void testGetByRegNumber() {
        CarInfo car = carService.getByRegNumber("AEX-314-MC");
        assertEquals("Branco", car.getColor());
    }

    @Test
    void testGetAll() {
        assertFalse(carService.getAll().isEmpty());
        assertEquals(2, carService.getAll().size());
    }

    @Test
    void testUpdate() {
        CarInfo oldCar = carService.getByRegNumber("AEX-313-MC");
        CarInfo newCar = new CarInfo("ABC-123-MP", "Mercedes", "Benz", "Preto");
        carService.update(oldCar.getRegistrationNr(), newCar);
        assertNotNull(carService.getByRegNumber("ABC-123-MP"));

    }

    @Test
    void testDelete() {
        carService.delete("AEX-312-MC");
        assertThrows(Exception.class, () ->{
            carService.getByRegNumber("AEX-312-MC");
        });
    }

}