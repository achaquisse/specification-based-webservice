package com.achaquisse.util;

import com.achaquisse.persistence.EntityManagerProducer;
import mz.co.talkcode.drawSQL.DrawSQL;
import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.BeforeEach;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Americo Chaquisse
 */
@EnableWeld
public abstract class BaseTestClass {

    @Inject
    private EntityManagerProducer producer;

    @WeldSetup
    public WeldInitiator weld = WeldInitiator.from(WeldInitiator.createWeld()
            .enableDiscovery()).activate(RequestScoped.class, SessionScoped.class, ApplicationScoped.class).build();

    static {
        System.setProperty("database.drop-create", "true");
        System.setProperty("connection.url","jdbc:h2:mem:test");
        System.setProperty("connection.driver_class","org.h2.Driver");
        System.setProperty("hibernate.dialect","org.hibernate.dialect.H2Dialect");
        System.setProperty("connection.show_sql","true");
        System.setProperty("connection.format_sql","true");
        System.setProperty("connection.enable.dev.pu","true");
    }

    @BeforeEach
    public void beforeEach(){
        producer.createEntityManager();
        loadDatabase();
    }

    protected void loadDatabase(){
        loadDatabase("test-data/default-model.dsql");
    }

    private void loadDatabase(String path){

        try {
            Connection connection = DriverManager.getConnection("jdbc:h2:mem:test");
            DrawSQL drawQL = new DrawSQL.Builder().fromSketch(getFile(path)).build();
            drawQL.apply(connection);

        } catch (Exception ex) {
            ex.printStackTrace();
            fail(String.format("Failed to load test data file %s", ex.getMessage()));
        }

    }

    private String getFile(String fileName) {

        StringBuilder result = new StringBuilder();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(fileName);

        try{

            InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader);
            for (String line; (line = reader.readLine()) != null;) {
                result.append(line).append("\n");
            }

            reader.close();
            streamReader.close();
        } catch (IOException ex){
            throw new RuntimeException(ex);
        }

        return result.toString();
    }
}
