package com.achaquisse.util;

import org.microshed.testing.SharedContainerConfiguration;
import org.microshed.testing.testcontainers.ApplicationContainer;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;

public class ApplicationConfig implements SharedContainerConfiguration {

    @Container
    public static MariaDBContainer<?> mysql = new MariaDBContainer<>()
            .withNetworkAliases("mariadb")
            .withExposedPorts(3306)
            .withUsername("admin")
            .withPassword("senha123")
            .withDatabaseName("car_db")
            .withInitScript("it-db/car_db.sql");

    @Container
    public static ApplicationContainer app = new ApplicationContainer()
            .withAppContextRoot("/car-store-api/");

}
