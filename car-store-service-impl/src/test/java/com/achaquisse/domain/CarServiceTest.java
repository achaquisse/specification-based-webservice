package com.achaquisse.domain;

import com.achaquisse.info.CarInfo;
import com.achaquisse.util.BaseTestClass;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

public class CarServiceTest extends BaseTestClass {

    @Inject
    private CarService carService;

    @Test
    public void testGetAll() {
        List<CarInfo> carInfos = carService.getAll();
        assertEquals(3, carInfos.size());
    }

    @Test
    public void testGetByRegNumber() {
        CarInfo carInfo = carService.getByRegNumber("AEX-313-MC");
        assertEquals("AEX-313-MC", carInfo.getRegistrationNr());
    }

    @Test
    public void testCreate() {
        CarInfo carInfo = new CarInfo("ABC-123-MZ", "Mercedes", "Benz", "Preto");
        carService.create(carInfo);
        assertEquals(4, carService.countAll());
    }

    @Test
    public void testUpdate() {
        CarInfo carInfo = carService.getByRegNumber("AEX-313-MC");
        assertEquals("Azul", carInfo.getColor());
        CarInfo updatedCarInfo = new CarInfo(carInfo.getRegistrationNr(), "Toyota", "Allion", "Castanho");
        carService.update("AEX-313-MC", updatedCarInfo);
        assertEquals("Castanho", carService.getByRegNumber("AEX-313-MC").getColor());
    }

    @Test
    public void testDelete() {
        carService.delete("AEX-313-MC");
        assertEquals(2, carService.countAll());
    }

}